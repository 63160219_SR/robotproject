/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robotproject2;

import java.util.Scanner;

/**
 *
 * @author sairu
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner wr = new Scanner(System.in);
        TableMap map = new TableMap(10,10);
        Robot robot = new Robot(2,2,'w',map);
        Boom boom = new Boom(5,5);
        map.setRobot(robot);
        map.setBoom(boom);
        while(true){
            map.showMap();
            char direction = inputDirection(wr);
            if(direction=='q'){
                printbyebye();
                break;
            }
            robot.walk(direction);
        }
    }

    private static void printbyebye() {
        System.out.println("ByeBye");
    }

    private static char inputDirection(Scanner wr) {
        String str = wr.next();
        char direction = str.charAt(0);
        return direction;
    }
}
