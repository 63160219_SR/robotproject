/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robotproject2;

/**
 *
 * @author sairu
 */
public class TableMap {
    private int width;
    private int height;
    private Robot robot;
    private Boom boom;

    public TableMap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
    }

    public void setBoom(Boom boom) {
        this.boom = boom;
    }
    public void showMap(){
        showtitel();
        for (int y = 0;y<width;y++){
            for(int x = 0;x<height;x++){
                if(robot.isOn(x, y)){
                    showRobot();
                }else if(boom.isOn(x, y)){
                    showboom();
                }else{
                    showcell();
                }
            }
            Enter();
        }
    }

    private void showtitel() {
        System.out.println("Map");
    }

    private void Enter() {
        System.out.println("");
    }

    private void showcell() {
        System.out.print("-");
    }

    private void showboom() {
        System.out.print(boom.getSymbol());
    }

    private void showRobot() {
        System.out.print(robot.getSymbol());
    }
    public boolean inMap(int x,int y){
        return !(x < 0 || x >= width || y < 0 || y >= height);
    }
    public boolean inBoom(int x,int y){
        return boom.isOn(x, y);
    }
}
